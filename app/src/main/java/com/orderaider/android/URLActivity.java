package com.orderaider.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.orderaider.android.model.URLGenerator;
import com.orderaider.android.services.APIHelper;
import com.orderaider.android.services.APIsInterface;
import com.orderaider.android.services.ServiceGenerator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class URLActivity extends Activity {
    LinearLayout submit_layout;
    EditText et_clientcode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_url);
        submit_layout = findViewById(R.id.submit_layout);
        et_clientcode = findViewById(R.id.et_clientcode);

        submit_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_clientcode.getText().length() > 0) {
//                    getURL();
                } else {
                    Toast.makeText(URLActivity.this, "Please enter client code that was provided", Toast.LENGTH_SHORT).show();
                }


            }
        });
    }

//    private void getURL() {
//        JsonObject jsonObject = new JsonObject();
//        jsonObject.addProperty("clients_clientcode", et_clientcode.getText().toString());
//        APIsInterface _APIsInterface = ServiceGenerator.createService(APIsInterface.class, Utils.URL_SERVER_ADDRESS);
//        Call<JsonObject> jsonObjectCall = _APIsInterface.CreateUser(jsonObject);
//        APIHelper.enqueueWithRetry(jsonObjectCall, 5, new Callback<JsonObject>() {
//            @Override
//            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                if (response.isSuccessful() && response.code() == 200) {
//                    Gson gson = new Gson();
//                    URLGenerator urlGenerator = gson.fromJson(response.body().toString(), URLGenerator.class);
//                    if (urlGenerator.status.equalsIgnoreCase("200")) {
//                        Intent intent = new Intent(URLActivity.this, MainActivity.class);
//                        intent.putExtra("GENERATED_URL", urlGenerator.objURLGenerator.table.get(0).clientsServiceUrl);
//                        startActivity(intent);
//                        finish();
//                    } else {
//                        Toast.makeText(URLActivity.this, urlGenerator.message, Toast.LENGTH_SHORT).show();
//                    }
//                } else {
//                    Toast.makeText(URLActivity.this, "Something went wrong, please check clint ID", Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//
//            }
//        });
//    }
}
