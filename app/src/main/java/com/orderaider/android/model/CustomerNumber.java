
package com.orderaider.android.model;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerNumber implements Serializable
{

    @SerializedName("DisplayMember")
    @Expose
    public String displayMember;
    @SerializedName("ID")
    @Expose
    public String iD;
    @SerializedName("ValueMember")
    @Expose
    public String valueMember;
    @SerializedName("Bold")
    @Expose
    public String bold;
    @SerializedName("FontSize")
    @Expose
    public String fontSize;
    @SerializedName("Align")
    @Expose
    public String align;
    @SerializedName("Font")
    @Expose
    public String font;
    private final static long serialVersionUID = 265903385630095805L;

}
