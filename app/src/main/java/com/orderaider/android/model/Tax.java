package com.orderaider.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Tax implements Serializable {

    @SerializedName("dblCalculatedTax")
    @Expose
    public Float dblCalculatedTax;
    @SerializedName("dblTaxValue")
    @Expose
    public Integer dblTaxValue;
    @SerializedName("TaxType")
    @Expose
    public String taxType;
    @SerializedName("TaxTypeId")
    @Expose
    public Integer taxTypeId;
    @SerializedName("isTaxable")
    @Expose
    public Boolean isTaxable;
    @SerializedName("BranchID")
    @Expose
    public Integer branchID;
    @SerializedName("SubOrgID")
    @Expose
    public Integer subOrgID;
    @SerializedName("POSUserID")
    @Expose
    public Integer pOSUserID;
    @SerializedName("ConnectionID")
    @Expose
    public Integer connectionID;
    @SerializedName("SectionId")
    @Expose
    public Integer sectionId;
    @SerializedName("Server")
    @Expose
    public Object server;
    @SerializedName("Username")
    @Expose
    public Object username;
    @SerializedName("Password")
    @Expose
    public Object password;
    @SerializedName("Database")
    @Expose
    public Object database;
    @SerializedName("PaymentStatus")
    @Expose
    public Object paymentStatus;
    @SerializedName("SqlAuthentication")
    @Expose
    public Integer sqlAuthentication;
    @SerializedName("LanguageId")
    @Expose
    public Integer languageId;
    private final static long serialVersionUID = -4948598498640549848L;
}

