
package com.orderaider.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Objresult_ implements Serializable {

    @SerializedName("ClientId")
    @Expose
    public Integer clientId;
    @SerializedName("OrderNo")
    @Expose
    public String orderNo;
    @SerializedName("BillingId")
    @Expose
    public Integer billingId;
    @SerializedName("PrintName")
    @Expose
    public String printName;
    @SerializedName("PrintLayout")
    @Expose
    public PrintLayout printLayout;
    private final static long serialVersionUID = 2342180200337968435L;



}
