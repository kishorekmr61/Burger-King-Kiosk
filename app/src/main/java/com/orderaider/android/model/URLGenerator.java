package com.orderaider.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class URLGenerator implements Serializable {

    @SerializedName("Status")
    @Expose
    public String status;
    @SerializedName("Message")
    @Expose
    public String message;
    @SerializedName("objresult")
    @Expose
    public ObjURLGenerator objURLGenerator;
    private final static long serialVersionUID = -3975430840069877157L;
}
