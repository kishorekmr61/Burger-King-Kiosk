
package com.orderaider.android.model;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Objresult implements Serializable
{

    @SerializedName("Status")
    @Expose
    public String status;
    @SerializedName("Message")
    @Expose
    public String message;
    @SerializedName("objresult")
    @Expose
    public Objresult_ objresult;
    private final static long serialVersionUID = 1012677557011618488L;

}
