package com.orderaider.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Objresult__ implements Serializable {
    @SerializedName("OfferText")
    @Expose
    public Object offerText;
    @SerializedName("OrganisationName")
    @Expose
    public OrganisationName organisationName;
    @SerializedName("BranchName")
    @Expose
    public OrganisationName branchName;
    @SerializedName("BranchAddress")
    @Expose
    public OrganisationName branchAddress;
    @SerializedName("RptField")
    @Expose
    public Object rptField;
    @SerializedName("City")
    @Expose
    public OrganisationName city;
    @SerializedName("Phoneno1")
    @Expose
    public OrganisationName phoneno1;
    @SerializedName("GSTIN")
    @Expose
    public OrganisationName gSTIN;
    @SerializedName("TaxInvoice")
    @Expose
    public OrganisationName taxInvoice;
    @SerializedName("Section")
    @Expose
    public Section section;
    @SerializedName("Date")
    @Expose
    public Date date;
    @SerializedName("BillNo")
    @Expose
    public OrganisationName billNo;
    @SerializedName("OrderNo")
    @Expose
    public OrderNo orderNo;
    @SerializedName("KitchenName")
    @Expose
    public KitchenName kitchenName;
    @SerializedName("Body")
    @Expose
    public Object body;
    @SerializedName("SubTotal")
    @Expose
    public OrganisationName subTotal;
    @SerializedName("DiscountAmount")
    @Expose
    public Object discountAmount;
    @SerializedName("OriginalAmout")
    @Expose
    public Object originalAmout;
    @SerializedName("Taxes")
    @Expose
    public List<Tax> taxes;
    @SerializedName("clsOrderedItems")
    @Expose
    public List<ClsOrderedItem> clsOrderedItems = null;
    @SerializedName("Total")
    @Expose
    public OrganisationName total;
    @SerializedName("TemplateNm")
    @Expose
    public OrganisationName templateNm;
    @SerializedName("SettleBy")
    @Expose
    public SettleBy settleBy;
    @SerializedName("CustomerName")
    @Expose
    public CustomerName customerName;
    @SerializedName("CustomerNumber")
    @Expose
    public CustomerNumber customerNumber;
    @SerializedName("CustomerAddress")
    @Expose
    public Object customerAddress;
    private final static long serialVersionUID = 7810794545902672722L;
}
