
package com.orderaider.android.model;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PrintModel implements Serializable
{

    @SerializedName("Status")
    @Expose
    public String status;
    @SerializedName("Message")
    @Expose
    public String message;
    @SerializedName("objresult")
    @Expose
    public List<Objresult_> objresult = null;
    private final static long serialVersionUID = 1168115399541692147L;

}
