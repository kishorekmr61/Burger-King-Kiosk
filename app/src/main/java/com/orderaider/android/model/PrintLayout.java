package com.orderaider.android.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PrintLayout implements Serializable {
    @SerializedName("Status")
    public String status;
    @SerializedName("Message")
    public String message;
    @SerializedName("objresult")
    public Objresult__ objresult;
    private final static long serialVersionUID = -1252253220818320399L;
}
