package com.orderaider.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Table implements Serializable {

    @SerializedName("client_accessToken")
    @Expose
    public String clientAccessToken;
    @SerializedName("clients_serviceUrl")
    @Expose
    public String clientsServiceUrl;
    @SerializedName("clients_seqno")
    @Expose
    public Integer clientsSeqno;
    @SerializedName("clients_clientID")
    @Expose
    public String clientsClientID;
    @SerializedName("clients_clientname")
    @Expose
    public String clientsClientname;
    @SerializedName("clients_contactno")
    @Expose
    public String clientsContactno;
    @SerializedName("clients_email")
    @Expose
    public String clientsEmail;
    @SerializedName("clients_IsActive")
    @Expose
    public Integer clientsIsActive;
    @SerializedName("clients_IsDeleted")
    @Expose
    public Integer clientsIsDeleted;
    @SerializedName("clients_createddate")
    @Expose
    public String clientsCreateddate;
    @SerializedName("clients_createdby")
    @Expose
    public Integer clientsCreatedby;
    @SerializedName("clients_country")
    @Expose
    public Integer clientsCountry;
    @SerializedName("clients_state")
    @Expose
    public Integer clientsState;
    @SerializedName("clients_city")
    @Expose
    public Integer clientsCity;
    @SerializedName("clients_address")
    @Expose
    public String clientsAddress;
    @SerializedName("clients_photo")
    @Expose
    public String clientsPhoto;
    @SerializedName("clients_ratings")
    @Expose
    public Integer clientsRatings;
    @SerializedName("clients_reviews")
    @Expose
    public Integer clientsReviews;
    @SerializedName("clients_serviceurl1")
    @Expose
    public String clientsServiceurl1;
    @SerializedName("clients_locationId")
    @Expose
    public Integer clientsLocationId;
    @SerializedName("clients_clientType")
    @Expose
    public Integer clientsClientType;
    @SerializedName("clients_disabled")
    @Expose
    public Boolean clientsDisabled;
    @SerializedName("clients_sequenceNo")
    @Expose
    public Integer clientsSequenceNo;
    @SerializedName("clients_locationId1")
    @Expose
    public Integer clientsLocationId1;
    private final static long serialVersionUID = 4201105320288768530L;

}
