
package com.orderaider.android.model;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClsOrderedItem implements Serializable
{

    @SerializedName("PrintName")
    @Expose
    public Object printName;
    @SerializedName("ItemType")
    @Expose
    public Object itemType;
    @SerializedName("Item4")
    @Expose
    public Object item4;
    @SerializedName("QP")
    @Expose
    public Object qP;
    @SerializedName("RecipeName")
    @Expose
    public String recipeName;
    @SerializedName("RecipeId")
    @Expose
    public Integer recipeId;
    @SerializedName("ComboId")
    @Expose
    public Integer comboId;
    @SerializedName("Qty")
    @Expose
    public Float qty;
    @SerializedName("Price")
    @Expose
    public Float price;
    @SerializedName("AddonAmount")
    @Expose
    public Float addonAmount;
    @SerializedName("BillingDetailId")
    @Expose
    public Integer billingDetailId;
    @SerializedName("KOTItemPrice")
    @Expose
    public Float kOTItemPrice;
    @SerializedName("SellingPrice")
    @Expose
    public Float sellingPrice;
    @SerializedName("Tax")
    @Expose
    public Float tax;
    @SerializedName("Section")
    @Expose
    public Object section;
    @SerializedName("Remarks")
    @Expose
    public String remarks;
    @SerializedName("Categories")
    @Expose
    public Object categories;
    @SerializedName("KOT")
    @Expose
    public Integer kOT;
    @SerializedName("DivisionID")
    @Expose
    public Integer divisionID;
    @SerializedName("MasterOrderNo")
    @Expose
    public Object masterOrderNo;
    @SerializedName("TotalQty")
    @Expose
    public Float totalQty;
    @SerializedName("OrderNo")
    @Expose
    public Object orderNo;
    @SerializedName("GrandTotal")
    @Expose
    public Float grandTotal;
    @SerializedName("NetTotal")
    @Expose
    public Float netTotal;
    @SerializedName("TotalTax")
    @Expose
    public Float totalTax;
    @SerializedName("TotalDiscount")
    @Expose
    public Float totalDiscount;
    @SerializedName("dblBalance")
    @Expose
    public Float dblBalance;
    @SerializedName("ClientId")
    @Expose
    public Integer clientId;
    @SerializedName("BillId")
    @Expose
    public Integer billId;
    private final static long serialVersionUID = 5821728636750799188L;

}
