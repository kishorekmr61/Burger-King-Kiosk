package com.orderaider.android.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import com.orderaider.android.MainActivity;
import com.orderaider.android.PrinterCallBack;

import java.util.Timer;
import java.util.TimerTask;

public class AlarmReceiver extends BroadcastReceiver {

     static PrinterCallBack printerCallBack;
     static Timer timer;

    @Override
    public void onReceive(Context context, Intent intent) {
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
              printerCallBack.printreceiveddata();
            }
        }, 0, 15000);//put here time 1000 milliseconds=1 second

    }


    public void setCallBack(PrinterCallBack mprinterCallBack){
        this.printerCallBack = mprinterCallBack;
    }
}
