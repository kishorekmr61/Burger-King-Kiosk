package com.orderaider.android.services;

import com.google.gson.JsonObject;
import com.orderaider.android.model.OrderResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIsInterface {

    @Headers("Content-Type: Application/json")
    @POST("consumer/api/v1/kiosk/printerKiosk")
    Call<OrderResponse> CreateUser(@Body JsonObject params);

}
