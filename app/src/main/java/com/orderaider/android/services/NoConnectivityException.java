package com.orderaider.android.services;

import java.io.IOException;

/**
 * Created by user on 20/8/18.
 */

public class NoConnectivityException extends IOException {

    @Override
    public String getMessage() {
        return "No Internet Connection";
    }

}
