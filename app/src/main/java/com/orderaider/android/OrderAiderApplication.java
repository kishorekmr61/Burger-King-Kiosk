package com.orderaider.android;

import android.app.Application;
import android.content.Context;
import android.widget.LinearLayout;

public class OrderAiderApplication extends Application {
    private static Context context;

    public static Context getAppContext() {
        return OrderAiderApplication.context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        OrderAiderApplication.context = getApplicationContext();
    }
}
