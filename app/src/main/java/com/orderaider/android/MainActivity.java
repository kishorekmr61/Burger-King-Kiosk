package com.orderaider.android;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.orderaider.android.model.OrderResponse;
import com.orderaider.android.model.PrintModel;
import com.orderaider.android.services.APIHelper;
import com.orderaider.android.services.APIsInterface;
import com.orderaider.android.services.AlarmReceiver;
import com.orderaider.android.services.ServiceGenerator;
import com.printsdk.cmd.PrintCmd;
import com.printsdk.usbsdk.UsbDriver;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends Activity implements PrinterCallBack {

    static UsbDevice mUsbDev1;
    static UsbDevice mUsbDev2;
    static UsbDriver mUsbDriver;
    private static UsbManager mUsbManager;
    private static final String ACTION_USB_PERMISSION = "com.usb.sample.USB_PERMISSION";
    private final static int PID11 = 8211;
    private final static int PID13 = 8213;
    private final static int PID15 = 8215;
    private final static int VENDORID = 1305;
    String normal = "", notConnectedOrNotPopwer = "", notMatch = "",
            printerHeadOpen = "", cutterNotReset = "", printHeadOverheated = "",
            blackMarkError = "", paperExh = "", paperWillExh = "", abnormal = "";
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    private static int cutter = 1;
    ArrayList<PrintModel> printModelArrayList;
    //    TextView htText;
    static EditText edittext, urledittext;
    TextView testprint;
    Button print_btn;
    String URL;
    UsbReceiver mUsbReceiver;
    String Deviceid;
    String url = "https://burgerkinguat.in/api/";
    public static final String MyPREFERENCES = "MyPrefs";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final SharedPreferences sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedpreferences.edit();
        sharedpreferences.getString("DeviceId", "");
        edittext = findViewById(R.id.edittext);
        urledittext = findViewById(R.id.urledittext);
        print_btn = findViewById(R.id.print_btn);
        testprint = findViewById(R.id.testprint);
//        init();
        String android_id = Secure.getString(getApplicationContext().getContentResolver(),
                Secure.ANDROID_ID);
        if (sharedpreferences.contains("DeviceId")) {
            edittext.setText(sharedpreferences.getString("DeviceId", ""));
        } else {
            edittext.setText(android_id);
        }
        if (sharedpreferences.contains("BASEURL")) {
            urledittext.setText(sharedpreferences.getString("BASEURL", ""));
        } else {
            urledittext.setText(url);
        }


        print_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("DeviceId", edittext.getText().toString());
                editor.putString("BASEURL", urledittext.getText().toString());
                editor.commit();
                if (!url.isEmpty()) {
                    Deviceid = edittext.getText().toString();
                    if (!Deviceid.isEmpty()) {
                        Utils.URL_SERVER_ADDRESS = urledittext.getText().toString();
                        init();
                        moveTaskToBack(true);
                    } else {
                        Toast.makeText(MainActivity.this, "Please enter Url", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Please enter Device ID", Toast.LENGTH_SHORT).show();
                }
            }
        });
        testprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUsbDriverService();
                if (!printConnStatus()) {
                    Toast.makeText(getApplicationContext(), "connect printer", Toast.LENGTH_SHORT).show();
                    return;
                }
                startPrint("Test Print");
            }
        });
    }

    private void init() {
        getUsbDriverService();
        boolean isWorking = isAlaramManagerExist();
        if (!isWorking) {
            startAlarm();
        }
        if (!printConnStatus()) {
            Toast.makeText(this, "connect printer", Toast.LENGTH_SHORT).show();
            return;
        }

    }

    private void getUsbDriverService() {
        mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        mUsbDriver = new UsbDriver(mUsbManager, this);
        PendingIntent permissionIntent1 = PendingIntent.getBroadcast(this, 0,
                new Intent(ACTION_USB_PERMISSION), 0);
        mUsbDriver.setPermissionIntent(permissionIntent1);
        // Broadcast listen for new devices

        mUsbReceiver = new UsbReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        this.registerReceiver(mUsbReceiver, filter);
    }

    @Override
    public void printreceiveddata() {
        sendConfirmation();
    }

    /*
     *  BroadcastReceiver when insert/remove the device USB plug into/from a USB port
     *  创建一个广播接收器接收USB插拔信息：当插入USB插头插到一个USB端口，或从一个USB端口，移除装置的USB插头
     */
    class UsbReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
                if (mUsbDriver.usbAttached(intent)) {
                    UsbDevice device = (UsbDevice) intent
                            .getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if ((device.getProductId() == PID11 && device.getVendorId() == VENDORID)
                            || (device.getProductId() == PID13 && device.getVendorId() == VENDORID)
                            || (device.getProductId() == PID15 && device.getVendorId() == VENDORID)) {
                        if (mUsbDriver.openUsbDevice(device)) {
                            if (device.getProductId() == PID11) {
                                mUsbDev1 = device;
//                                mUsbDev = mUsbDev1;
                            } else {
                                mUsbDev2 = device;
//                                mUsbDev = mUsbDev2;
                            }
                        }
                    }
                }
            } else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                UsbDevice device = (UsbDevice) intent
                        .getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if ((device.getProductId() == PID11 && device.getVendorId() == VENDORID)
                        || (device.getProductId() == PID13 && device.getVendorId() == VENDORID)
                        || (device.getProductId() == PID15 && device.getVendorId() == VENDORID)) {
                    mUsbDriver.closeUsbDevice(device);
                    if (device.getProductId() == PID11)
                        mUsbDev1 = null;
                    else
                        mUsbDev2 = null;
                }
            } else if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if ((device.getProductId() == PID11 && device.getVendorId() == VENDORID)
                                || (device.getProductId() == PID13 && device.getVendorId() == VENDORID)
                                || (device.getProductId() == PID15 && device.getVendorId() == VENDORID)) {
                            if (mUsbDriver.openUsbDevice(device)) {
                                if (device.getProductId() == PID11) {
                                    mUsbDev1 = device;
//                                    mUsbDev = mUsbDev1;
                                } else {
                                    mUsbDev2 = device;
//                                    mUsbDev = mUsbDev2;
                                }
                            }
                        }
                    } else {
                        T.showShort(MainActivity.this, "permission denied for device");
                        //Log.d(TAG, "permission denied for device " + device);
                    }
                }
            }
        }
    }

    private int getPrinterStatus(UsbDevice usbDev) {
        int iRet = -1;

        byte[] bRead1 = new byte[1];
        byte[] bWrite1 = PrintCmd.GetStatus1();
        if (mUsbDriver.read(bRead1, bWrite1, usbDev) > 0) {
            iRet = PrintCmd.CheckStatus1(bRead1[0]);
        }

        if (iRet != 0)
            return iRet;

        byte[] bRead2 = new byte[1];
        byte[] bWrite2 = PrintCmd.GetStatus2();
        if (mUsbDriver.read(bRead2, bWrite2, usbDev) > 0) {
            iRet = PrintCmd.CheckStatus2(bRead2[0]);
        }

        if (iRet != 0)
            return iRet;

        byte[] bRead3 = new byte[1];
        byte[] bWrite3 = PrintCmd.GetStatus3();
        if (mUsbDriver.read(bRead3, bWrite3, usbDev) > 0) {
            iRet = PrintCmd.CheckStatus3(bRead3[0]);
        }

        if (iRet != 0)
            return iRet;

        byte[] bRead4 = new byte[1];
        byte[] bWrite4 = PrintCmd.GetStatus4();
        if (mUsbDriver.read(bRead4, bWrite4, usbDev) > 0) {
            iRet = PrintCmd.CheckStatus4(bRead4[0]);
        }


        return iRet;
    }


    private int checkStatus(int iStatus) {
        int iRet = -1;

        StringBuilder sMsg = new StringBuilder();


        //0 打印机正常 、1 打印机未连接或未上电、2 打印机和调用库不匹配
        //3 打印头打开 、4 切刀未复位 、5 打印头过热 、6 黑标错误 、7 纸尽 、8 纸将尽
        switch (iStatus) {
            case 0:
                sMsg.append(normal);       // 正常
                iRet = 0;
                break;
            case 8:
                sMsg.append(paperWillExh); // 纸将尽
                iRet = 0;
                break;
            case 3:
                sMsg.append(printerHeadOpen); //打印头打开
                break;
            case 4:
                sMsg.append(cutterNotReset);
                break;
            case 5:
                sMsg.append(printHeadOverheated);
                break;
            case 6:
                sMsg.append(blackMarkError);
                break;
            case 7:
                sMsg.append(paperExh);     // 纸尽==缺纸
                break;
            case 1:
                sMsg.append(notConnectedOrNotPopwer);
                break;
            default:
                sMsg.append(abnormal);     // 异常
                break;
        }
//        Toast.makeText(this, sMsg.toString(), Toast.LENGTH_SHORT).show();
        return iRet;

    }

    public String loadJSONFromAsset(String name) {
        String json;
        try {
            InputStream is = getAssets().open(name);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            //ex.printStackTrace();
            return null;
        }
        return json;
    }


    public void sendConfirmation() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("printer_device_id", edittext.getText().toString());
        APIsInterface _APIsInterface = ServiceGenerator.createService(APIsInterface.class, Utils.URL_SERVER_ADDRESS);
        Call<OrderResponse> jsonObjectCall = _APIsInterface.CreateUser(jsonObject);
        APIHelper.enqueueWithRetry(jsonObjectCall, 5, new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                if (response.body() != null && response.body().getError() == 0) {
                    if (response.body().getContent() != null) {
                        startPrint(response.body().getContent().getReponseText());
//                    orderids.add(response.body().getContent().getRequestOrderId());
                    }
                }
                if (response.body() != null) {
                    Log.d("Success", "testing" + response.body().getContent().getRequestOrderId());
                } else {
                    Log.d("Success", "testing");
                }
            }

            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                Log.d("Error", "testing");
            }
        });

    }

    private void startPrint(String message) {
        if (mUsbDriver != null) {
            printConnStatus();
            printData(message);
        } else {
            getUsbDriverService();
            printConnStatus();
            if (mUsbDriver != null) {
                printData(message);
            }
        }
    }


    public void printData(String message) {
        mUsbDriver.write(PrintCmd.SetLeftmargin(5), mUsbDev1);
        mUsbDriver.write(PrintCmd.SetRightmargin(5), mUsbDev1);
        mUsbDriver.write(PrintCmd.PrintString(message, 0), mUsbDev1);
        setFeedCut(cutter, mUsbDev1);
    }

    //Feed paper, cut paper, clear cache
    private void setFeedCut(int iMode, UsbDevice usbDev) {
        mUsbDriver.write(PrintCmd.PrintFeedline(5), usbDev);      // Paper feed
        mUsbDriver.write(PrintCmd.PrintCutpaper(iMode), usbDev);  // Cut paper type
    }


    // Get UsbDriver(UsbManager) service
    private boolean printConnStatus() {
        boolean blnRtn = false;
        try {
            if (!mUsbDriver.isConnected()) {
                // USB线已经连接
                for (UsbDevice device : mUsbManager.getDeviceList().values()) {
                    if ((device.getProductId() == PID11 && device.getVendorId() == VENDORID)
                            || (device.getProductId() == PID13 && device.getVendorId() == VENDORID)
                            || (device.getProductId() == PID15 && device.getVendorId() == VENDORID)) {
//						if (!mUsbManager.hasPermission(device)) {
//							break;
//						}
                        blnRtn = mUsbDriver.usbAttached(device);
                        if (blnRtn == false) {
                            break;
                        }
                        blnRtn = mUsbDriver.openUsbDevice(device);

                        // 打开设备
                        if (blnRtn) {
                            if (device.getProductId() == PID11) {
                                mUsbDev1 = device;
//                                mUsbDev = mUsbDev1;
                            } else {
                                mUsbDev2 = device;
//                                mUsbDev = mUsbDev2;
                            }
                            T.showShort(this, "USB Success");
                            break;
                        } else {
                            T.showShort(this, "USB Failed");
                            break;
                        }
                    }
                }
            } else {
                blnRtn = true;
            }
        } catch (Exception e) {
            T.showShort(this, e.getMessage());
        }
        return blnRtn;
    }

    private void setClean() {
        mUsbDriver.write(PrintCmd.SetClean());// 清除缓存,初始化
    }


    private void startAlarm() {
        AlarmManager manager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        Intent myIntent = new Intent(this, AlarmReceiver.class);
        Calendar updateTime = Calendar.getInstance();
        updateTime.set(Calendar.SECOND, 10);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1234, myIntent, 0);
        manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() , 15 * 1000, pendingIntent);
        AlarmReceiver alarmReceiver = new AlarmReceiver();
        alarmReceiver.setCallBack(MainActivity.this);
    }

    public boolean isAlaramManagerExist() {
        Intent myIntent = new Intent(MainActivity.this, AlarmReceiver.class);
        return (PendingIntent.getBroadcast(MainActivity.this, 1234, myIntent, PendingIntent.FLAG_NO_CREATE) != null);
    }
}

